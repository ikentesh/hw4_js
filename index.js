      /*Теоретичні питання

Описати своїми словами навіщо потрібні функції у програмуванні.

    Функція – це блок коду, призначений для виконання певного завдання. Функції дозволяють повторно використовувати код,
необхідно лише один раз задати блок коду і потім можна використовувати його скільки завгодно разів з будь-якого місця
скрипта з різними аргументами, щоб отримати різні результати.

Описати своїми словами, навіщо у функцію передавати аргумент.

    Функції можна задати скільки завгодно параметрів, а потім надавати їй аргументи і виводити результат на екран.
Якщо під час виклику функції не вказати аргументи, то за замовчуванням повернеться значення undefined.

Що таке оператор return та як він працює всередині функції?

    Оператор return використовується у функціях для повернення даних після виконання роботи функції.
Оператор return зупининяє роботу функції і повертає значення, яке знаходиться праворуч від нього.
Значення може бути у вигляді однієї змінної, а може бути й виразом (кілька змінних, між якими є інші оператори –
додавання, множення тощо).*/


      //Завдання

let firstNumber;
let calcOperation;
let secondNumber;

do {
  firstNumber = +prompt("Enter first number", firstNumber);
  calcOperation = prompt("Choose operation (+, -, * or /)", "");
  secondNumber = +prompt("Enter second number", secondNumber);
} while (
  !firstNumber ||
  !secondNumber ||
  !calcOperation ||
  !(
    calcOperation == "+" ||
    calcOperation == "-" ||
    calcOperation == "*" ||
    calcOperation == "/"
  )
);

function result(a, b) {
  switch (calcOperation) {
    case "+":
      return a + b;
      break;
    case "-":
      return a - b;
      break;
    case "*":
      return a * b;
      break;
    case "/":
      return a / b;
      break;
  }
}

console.log(result(firstNumber, secondNumber));
